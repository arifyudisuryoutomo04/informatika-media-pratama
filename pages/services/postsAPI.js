import instance from "../axios/axiosGlobal";

const getPosts = () => {
  return instance.get(`posts`);
};

const getDetailPosts = (id) => {
  return instance.get(`posts/${id}`);
};

const getDetailPhotos = (id) => {
  return instance.get(`photos/${id}`);
};

const getDetailComment = (prams) => {
  return instance.get(`comments/`, prams);
};

const getPhotos = () => {
  return instance.get(`photos`);
};

const deletePosts = (id) => {
  return instance.delete(`posts/${id}`);
};

const updatePosts = (id, data) => {
  return instance.patch(`posts/${id}`, data);
};

const postsAPIs = {
  getPosts,
  getPhotos,
  deletePosts,
  updatePosts,
  getDetailPosts,
  getDetailPhotos,
  getDetailComment,
};

export default postsAPIs;
