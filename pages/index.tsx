import {
  Card,
  Container,
  CardBody,
  Image,
  Stack,
  Heading,
  Button,
  Box,
  useColorModeValue,
  SimpleGrid,
} from "@chakra-ui/react";
import { useState, useEffect } from "react";
import postsAPIs from "./services/postsAPI";
import SweetAlert from "react-bootstrap-sweetalert";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";

function Home() {
  const [showAlert, setShowAlert] = useState<Boolean>(false);
  const [alertType, setAlerType] = useState<number>(1);
  const [alertMessage, setAlertMessage] = useState<string>("");
  const [data, setData] = useState([]);
  const router = useRouter();

  const loadData = async () => {
    const postPromise = postsAPIs.getPosts();
    const photosPromise = postsAPIs.getPhotos();

    Promise.all([postPromise, photosPromise])
      .then(([postResponse, photosResponse]) => {
        const post = postResponse?.data;
        const photos = photosResponse?.data;
        const findData = [];

        post.forEach((item) => {
          const _photos = photos.find((v) => v.id === item.id);

          if (_photos) {
            const newObj = { ...item, ..._photos };
            findData.push(newObj);
          }
        });

        setData(findData);
      })
      .catch((error) => {
        setShowAlert(true);
        setAlerType(2);
        setAlertMessage("Failed to Load data");
      });
  };

  useEffect(() => {
    loadData();
  }, []);

  const hideAlert = () => {
    setShowAlert(false);
    loadData();
  };

  const handleDetail = (id: String) => {
    router.push(`/posts/${id}`);
  };

  const handleDelete = async (id: String) => {
    await postsAPIs
      .deletePosts(id)
      .then((res) => {
        if (res?.status === 200) {
          setShowAlert(true);
          setAlerType(1);
          setAlertMessage("Success Deleted");
        } else {
          setShowAlert(true);
          setAlerType(2);
          setAlertMessage("Failed Deleted");
        }
      })
      .catch((err) => {
        setShowAlert(true);
        setAlerType(2);
        setAlertMessage("Failed Deleted");
      });
  };

  const checkTextLength = (text, maxLength) => {
    return text.length > maxLength
      ? { whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }
      : {};
  };

  return (
    <Container maxW={"9xl"} py={12}>
      <SweetAlert
        show={showAlert}
        confirmBtnBsStyle={alertType === 1 ? "success" : "warning"}
        confirmBtnText="Close"
        title={alertType === 1 ? "Succes!" : "Invalid!"}
        onConfirm={hideAlert}
        warning={alertType === 2}
        success={alertType === 1}
        style={{
          color: useColorModeValue("white", "dark") === "dark" ? "black" : null,
        }}
      >
        {alertMessage}
      </SweetAlert>
      <Box display="flex" flexWrap="wrap" justifyContent="center">
        {!isEmpty(data) &&
          data.map((v: any, i) => {
            return (
              <Card style={{ width: "20%", margin: "1rem" }} key={i}>
                <CardBody>
                  <Image
                    src={v?.url}
                    alt="Green double couch with wooden legs"
                    borderRadius="lg"
                  />
                  <Stack mt="6" spacing="3">
                    <Heading size="md" height={50}>
                      {v.title}
                    </Heading>
                    <div style={{ ...checkTextLength(v?.body, 100) }}>
                      {v?.body}
                    </div>
                    <SimpleGrid columns={2} spacingX="40px" spacingY="20px">
                      <Button
                        color="blue.600"
                        fontSize="1xl"
                        onClick={() => handleDetail(v.id)}
                      >
                        Detail
                      </Button>
                      <Button
                        bgColor="red"
                        color="white"
                        fontSize="1xl"
                        onClick={() => handleDelete(v.id)}
                      >
                        Delete
                      </Button>
                    </SimpleGrid>
                  </Stack>
                </CardBody>
              </Card>
            );
          })}
      </Box>
    </Container>
  );
}

export default Home;
