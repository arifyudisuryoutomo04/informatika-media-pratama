import { useRouter } from "next/router";
import {
  Box,
  Container,
  Stack,
  Text,
  Image,
  Flex,
  Button,
  SimpleGrid,
  StackDivider,
  useColorModeValue,
  chakra,
  Avatar,
  background,
  FormControl,
  Input,
  Textarea,
} from "@chakra-ui/react";
import postsAPIs from "../services/postsAPI";
import SweetAlert from "react-bootstrap-sweetalert";
import { useState, useEffect } from "react";
import { isEmpty } from "lodash";
import { useFormik } from "formik";

const UserDetails = () => {
  const [showAlert, setShowAlert] = useState<Boolean>(false);
  const [alertType, setAlerType] = useState<number>(1);
  const [alertMessage, setAlertMessage] = useState<string>("");
  const [data, setData] = useState({});
  const [comment, setComment] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(10);

  const loadData = async () => {
    let params = { params: { postId: id } };
    await postsAPIs
      .getDetailPosts(id)
      .then((res) => {
        if (res?.status === 200) {
          const postData = res?.data;
          postsAPIs
            .getDetailPhotos(id)
            .then((res) => {
              if (res?.status === 200) {
                const photoData = res?.data;
                const findData = { ...postData, ...photoData };
                setData(findData);
              }
            })
            .catch(() => {
              setShowAlert(true);
              setAlerType(2);
              setAlertMessage("Failed to Load data");
            });
        }
      })
      .catch(() => {
        setShowAlert(true);
        setAlerType(2);
        setAlertMessage("Failed to Load data");
      });

    const commentPromise = postsAPIs.getDetailComment(params);
    const photosPromise = postsAPIs.getPhotos();

    Promise.all([commentPromise, photosPromise])
      .then(([postResponse, photosResponse]) => {
        const comment = postResponse?.data;
        const photos = photosResponse?.data;
        const findData = [];

        comment.forEach((item) => {
          const _photos = photos.find((v) => v.id === item.id);

          if (_photos) {
            const newObj = { ...item, ..._photos };
            findData.push(newObj);
          }
        });

        setComment(findData);
      })
      .catch((error) => {
        setShowAlert(true);
        setAlerType(2);
        setAlertMessage("Failed to Load data");
      });
  };

  useEffect(() => {
    loadData();
  }, []);

  const hideAlert = () => {
    setShowAlert(false);
    if (alertType === 1) return router.push("/");
  };

  const handleClickLoadMore = () => {
    setItemsPerPage((prevItemsPerPage) => prevItemsPerPage + 5);
  };

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const visibleComments = comment.slice(startIndex, endIndex);

  const formik = useFormik({
    initialValues: data,
    enableReinitialize: true,
    validateOnChange: true,
    validateOnBlur: true,
    onSubmit: async (values) => {
      let data = { title: values.title, body: values.body };
      await postsAPIs
        .updatePosts(id, data)
        .then((res) => {
          setShowAlert(true);
          setAlerType(1);
          setAlertMessage("Success to Updated");
        })
        .catch((err) => {
          setShowAlert(true);
          setAlerType(2);
          setAlertMessage("Failed to Updated");
        });
    },
  });

  return (
    <Container maxW={"7xl"}>
      <SweetAlert
        show={showAlert}
        confirmBtnBsStyle={alertType === 1 ? "success" : "warning"}
        confirmBtnText="Close"
        title={alertType === 1 ? "Succes!" : "Invalid!"}
        onConfirm={hideAlert}
        warning={alertType === 2}
        success={alertType === 1}
        style={{
          color: useColorModeValue("white", "dark") === "dark" ? "black" : null,
        }}
      >
        {alertMessage}
      </SweetAlert>
      <SimpleGrid
        columns={{ base: 1, lg: 2 }}
        spacing={{ base: 8, md: 10 }}
        py={{ base: 18, md: 24 }}
      >
        <Flex>
          <Image
            rounded={"md"}
            alt={"product image"}
            src={data?.url}
            fit={"cover"}
            align={"center"}
            w={"100%"}
            h={{ base: "100%", sm: "400px", lg: "500px" }}
          />
        </Flex>
        <form onSubmit={formik.handleSubmit}>
          <Stack spacing={{ base: 6, md: 10 }}>
            <Box as={"header"}>
              <Input
                lineHeight={1.1}
                fontWeight={600}
                name="title"
                fontSize={{ base: "2xl", sm: "3xl", lg: "3xl" }}
                value={formik.values.title}
                onChange={formik.handleChange}
                onBlur={formik.onBlur}
                required
              />
            </Box>

            <Stack
              spacing={{ base: 4, sm: 6 }}
              direction={"column"}
              divider={
                <StackDivider
                  borderColor={useColorModeValue("gray.200", "gray.600")}
                />
              }
            >
              <Box>
                <Text
                  fontSize={{ base: "16px", lg: "18px" }}
                  color={useColorModeValue("yellow.500", "yellow.300")}
                  fontWeight={"500"}
                  textTransform={"uppercase"}
                  mb={"4"}
                >
                  Description
                </Text>
                <FormControl>
                  <Textarea
                    required
                    name="body"
                    value={formik.values.body}
                    onChange={formik.handleChange}
                    onBlur={formik.onBlur}
                    placeholder="Here is a sample placeholder"
                  />
                </FormControl>
              </Box>
              <Flex
                textAlign={"center"}
                pt={10}
                justifyContent={"center"}
                direction={"column"}
                width={"full"}
                overflow={"hidden"}
              >
                <Box
                  width={{ base: "full", sm: "lg", lg: "xl" }}
                  margin={"auto"}
                >
                  <chakra.h3
                    fontFamily={"Work Sans"}
                    fontWeight={"bold"}
                    fontSize={20}
                    textTransform={"uppercase"}
                    color={"purple.400"}
                  >
                    Comment
                  </chakra.h3>
                </Box>
                <SimpleGrid>
                  {!isEmpty(visibleComments) &&
                    visibleComments.map((res, i) => {
                      return <TestimonialCard {...res} index={i} key={i} />;
                    })}
                </SimpleGrid>
                <Flex h="20vh" justifyContent="center" alignItems="center">
                  {endIndex < comment.length && (
                    <Button
                      fontSize={"sm"}
                      rounded={"full"}
                      bg={"blue.400"}
                      color={"white"}
                      onClick={handleClickLoadMore}
                      _hover={{
                        bg: "blue.500",
                      }}
                      _focus={{
                        bg: "blue.500",
                      }}
                    >
                      Load comment
                    </Button>
                  )}
                  <Button
                    type="submit"
                    ml={2}
                    fontSize={"sm"}
                    rounded={"full"}
                    bg={"blue.400"}
                    color={"white"}
                    _hover={{
                      bg: "blue.500",
                    }}
                    _focus={{
                      bg: "blue.500",
                    }}
                  >
                    Update
                  </Button>
                </Flex>
              </Flex>
            </Stack>
          </Stack>
        </form>
      </SimpleGrid>
    </Container>
  );
};

function TestimonialCard(props) {
  return (
    <Flex
      boxShadow={"lg"}
      maxW={"640px"}
      direction={{ base: "column-reverse", md: "row" }}
      width={"full"}
      rounded={"xl"}
      p={10}
      justifyContent={"space-between"}
      position={"relative"}
      bg={useColorModeValue("white", "gray.800")}
      _after={{
        content: '""',
        position: "absolute",
        height: "21px",
        width: "29px",
        left: "35px",
        top: "-10px",
        backgroundSize: "cover",
        backgroundImage: `url("data:image/svg+xml, %3Csvg xmlns='http://www.w3.org/2000/svg' width='29' height='21' viewBox='0 0 29 21' fill='none'%3E%3Cpath d='M6.91391 21C4.56659 21 2.81678 20.2152 1.66446 18.6455C0.55482 17.0758 0 15.2515 0 13.1727C0 11.2636 0.405445 9.43939 1.21634 7.7C2.0699 5.91818 3.15821 4.3697 4.48124 3.05454C5.84695 1.69697 7.31935 0.678787 8.89845 0L13.3157 3.24545C11.5659 3.96667 9.98676 4.94242 8.57837 6.17273C7.21266 7.36061 6.25239 8.63333 5.69757 9.99091L6.01766 10.1818C6.27373 10.0121 6.55114 9.88485 6.84989 9.8C7.19132 9.71515 7.63944 9.67273 8.19426 9.67273C9.34658 9.67273 10.4776 10.097 11.5872 10.9455C12.7395 11.7939 13.3157 13.1091 13.3157 14.8909C13.3157 16.8848 12.6542 18.4121 11.3311 19.4727C10.0508 20.4909 8.57837 21 6.91391 21ZM22.5982 21C20.2509 21 18.5011 20.2152 17.3488 18.6455C16.2391 17.0758 15.6843 15.2515 15.6843 13.1727C15.6843 11.2636 16.0898 9.43939 16.9007 7.7C17.7542 5.91818 18.8425 4.3697 20.1656 3.05454C21.5313 1.69697 23.0037 0.678787 24.5828 0L29 3.24545C27.2502 3.96667 25.6711 4.94242 24.2627 6.17273C22.897 7.36061 21.9367 8.63333 21.3819 9.99091L21.702 10.1818C21.9581 10.0121 22.2355 9.88485 22.5342 9.8C22.8756 9.71515 23.3238 9.67273 23.8786 9.67273C25.0309 9.67273 26.1619 10.097 27.2715 10.9455C28.4238 11.7939 29 13.1091 29 14.8909C29 16.8848 28.3385 18.4121 27.0155 19.4727C25.7351 20.4909 24.2627 21 22.5982 21Z' fill='%239F7AEA'/%3E%3C/svg%3E")`,
      }}
      _before={{
        content: '""',
        position: "absolute",
        zIndex: "-1",
        height: "full",
        maxW: "640px",
        width: "full",
        filter: "blur(40px)",
        transform: "scale(0.98)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        top: 0,
        left: 0,
        backgroundImage: background[props?.index % 4],
      }}
    >
      <Flex
        direction={"column"}
        textAlign={"left"}
        justifyContent={"space-between"}
      >
        <chakra.p fontFamily={"Inter"} fontWeight={"medium"} fontSize={"15px"}>
          {props?.name}
        </chakra.p>
        <chakra.p
          fontFamily={"Inter"}
          fontWeight={"medium"}
          fontSize={"15px"}
          pb={4}
        >
          {props?.email}
        </chakra.p>
        <chakra.p fontFamily={"Work Sans"} fontWeight={"bold"} fontSize={14}>
          {/* {name} */}
          <chakra.span
            fontFamily={"Inter"}
            fontWeight={"medium"}
            color={"gray.500"}
          >
            {" "}
            - {props?.body}
          </chakra.span>
        </chakra.p>
      </Flex>
      <Avatar
        src={props?.url}
        height={"80px"}
        width={"80px"}
        alignSelf={"center"}
        m={{ base: "0 0 35px 0", md: "0 0 0 50px" }}
      />
    </Flex>
  );
}

export default UserDetails;
